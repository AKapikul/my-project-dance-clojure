(ns my-project.core-test
  (:require [clojure.test :refer :all]
            [my-project.core :refer :all]
            [my-project.service.danceService :as danceService]
            [my-project.service.dancerService :as dancerService]
            [my-project.service.danceGroupService :as danceGroupService]
            [my-project.service.instructorService :as instructorGroupService]
            [my-project.service.userService :as userService]
            [toucan.db :as db]
            ))

(db/set-default-quoting-style! :mysql)

(db/set-default-db-connection!
  {
   :classname   "com.mysql.cj.jdbc.Driver"
   :subprotocol "mysql"
   :subname     "//localhost:3306/dancedb"
   :user        "root"
   :password    "password"
   :useSSL      false
   })

(deftest create-dance
  (testing "Test create dance"
    (let [dance (danceService/create-dance {:name "Salsa"
                                            :note "Couple dance from Cuba"
                                            })
          found-dance (danceService/get-dance ((dance :body) :id))]
      (is (= "Salsa" ((found-dance :body) :name)))
      )
    )
  )

(deftest delete-dance
  (testing "Test delete dance"
    (let [dance (danceService/create-dance {:name "Bachata"
                                            :note "Latino couple dance"})]
      (danceService/delete-dance ((dance :body) :id))
      (is (= nil ((danceService/get-dance ((dance :body) :id)) :body)))
      )))

(deftest update-:dance
  (testing "Test update dance"
    (let [dance (danceService/create-dance {:name "Salsa"
                                            :note "Couple dance of African workers"
                                            })
          update (danceService/update-dance ((dance :body) :id) {:name "Rumba"
                                                                 :note "Couple dance of African workers"
                                                                 })
          found-dance (danceService/get-dance (update :body))]
      (is (= "Rumba" ((found-dance :body) :name)))
      )
    )
  )

(deftest find-all-dances
  (testing "Find all dances"
    (def dancesList (count ((danceService/get-dances) :body)))
    (danceService/create-dance {:name "Rumba"
                              :note "Worker's couple dance from Cuba"})
    (is (= (inc dancesList) (count ((danceService/get-dances) :body))))
    )
  )




(deftest create-user
  (testing "Test create user"
    (let [user (userService/create-user {:username  "anjak"
                                         :password  "anjak97"
                                         :firstName "Anja"
                                         :lastName  "Kapikul"
                                         :email     "anja@gmail.com"})
          found-user (userService/get-user ((user :body) :id))]
      (is (= "anjak" ((found-user :body) :username)))
      )
    )
  )

(deftest delete-user
  (testing "Test delete user"
    (let [user (userService/create-user {:username  "anjak2"
                                         :password  "anjak972"
                                         :firstName "Anja2"
                                         :lastName  "Kapikul2"
                                         :email     "anja2@gmail.com"})]
      (userService/delete-user ((user :body) :id))
      (is (= nil ((userService/get-user ((user :body) :id)) :body)))
      )))

(deftest update-user
  (testing "Test update user"
    (let [user (userService/create-user {:username  "anjak"
                                         :password  "anjak97"
                                         :firstName "Anja"
                                         :lastName  "Kapikul"
                                         :email     "anja@gmail.com"})
          update (userService/update-user ((user :body) :id) {:username  "novi"
                                                              :password  "anjak97"
                                                              :firstName "Anja"
                                                              :lastName  "Kapikul"
                                                              :email     "anja@gmail.com"})
          found-user (userService/get-user (update :body))]
      (is (= "novi" ((found-user :body) :username)))
      )
    )
  )

(deftest find-all-users
  (testing "Find all users"
    (def usersList (count ((userService/get-users) :body)))
    (userService/create-user {:username  "anjak"
                              :password  "anjak97"
                              :firstName "Anja"
                              :lastName  "Kapikul"
                              :email     "anja@gmail.com"})
    (is (= (inc usersList) (count ((userService/get-users) :body))))
    )
  )