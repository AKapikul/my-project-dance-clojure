(defproject my-project "0.1.0-SNAPSHOT"
  :description "Check out README file!"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url  "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [prismatic/schema "1.1.12"]
                 [metosin/compojure-api "2.0.0-alpha30"]
                 [ring/ring-jetty-adapter "1.8.2"]
                 [ring-cors "0.1.13"]
                 [toucan "1.15.1"]
                 [mysql/mysql-connector-java "8.0.12"]
                 [buddy/buddy-hashers "1.7.0"]]
  :repl-options {:init-ns my-project.core}
  :main ^:skip-aot my-project.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
