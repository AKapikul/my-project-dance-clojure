(ns my-project.service.danceGroupService
  (:require [my-project.model.danceGroup :refer [DanceGroup]]
            [toucan.db :as db]
            [ring.util.http-response :refer [ok not-found created]]
            [buddy.hashers :as hashers]))

(defn id->created [id]
  (created (str "/danceGroups/" id) {:id id}))

(defn create-danceGroup [add-danceGroup-req]
  (->> (db/insert! DanceGroup add-danceGroup-req)
       :id
       id->created))

(defn get-danceGroups []
  (->> (db/select DanceGroup)
       ok))

(defn danceGroup->response [danceGroup]
  (if danceGroup
    (ok danceGroup)
    (not-found)))

(defn get-danceGroup [danceGroup-id]
  (-> (DanceGroup danceGroup-id)
      danceGroup->response))

(defn update-danceGroup [id updated-danceGroup]
  (db/update! DanceGroup id updated-danceGroup)
  (ok updated-danceGroup))

(defn delete-danceGroup [id]
  (db/delete! DanceGroup :id id)
  (ok))
