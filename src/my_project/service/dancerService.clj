(ns my-project.service.dancerService
  (:require [my-project.model.dancer :refer [Dancer]]
            [toucan.db :as db]
            [ring.util.http-response :refer [ok not-found created]]
            [buddy.hashers :as hashers]))

(defn id->created [id]
  (created (str "/dancers/" id) {:id id}))

(defn create-dancer [add-dancer-req]
  (->> (db/insert! Dancer add-dancer-req)
       :id
       id->created))

(defn get-dancers []
  (->> (db/select Dancer)
       ok))

(defn dancer->response [dancer]
  (if dancer
    (ok dancer)
    (not-found)))

(defn get-dancer [dancer-id]
  (-> (Dancer dancer-id)
      dancer->response))

(defn update-dancer [id updated-dancer]
  (db/update! Dancer id updated-dancer)
  (ok updated-dancer))

(defn delete-dancer [id]
  (db/delete! Dancer :id id)
  (ok))