(ns my-project.service.danceService
    (:require [my-project.model.dance :refer [Dance]]
      [toucan.db :as db]
      [ring.util.http-response :refer [ok not-found created]]
      [buddy.hashers :as hashers]))

(defn id->created [id]
      (created (str "/dances/" id) {:id id}))

(defn create-dance [add-dance-req]
      (->> (db/insert! Dance add-dance-req)
           :id
           id->created))

(defn get-dances []
      (->> (db/select Dance)
           ok))

(defn dance->response [dance]
      (if dance
        (ok dance)
        (not-found)))

(defn get-dance [dance-id]
      (-> (Dance dance-id)
          dance->response))

(defn update-dance [id updated-dance]
  (db/update! Dance id updated-dance)
  (ok updated-dance))

(defn delete-dance [id]
      (db/delete! Dance :id id)
      (ok))
