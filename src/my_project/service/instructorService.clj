(ns my-project.service.instructorService
  (:require [my-project.model.instructor :refer [Instructor]]
            [toucan.db :as db]
            [ring.util.http-response :refer [ok not-found created]]
            [buddy.hashers :as hashers]))

(defn id->created [id]
  (created (str "/instructors/" id) {:id id}))

(defn create-instructor [add-instructor-req]
  (->> (db/insert! Instructor add-instructor-req)
       :id
       id->created))

(defn get-instructors []
  (->> (db/select Instructor)
       ok))

(defn instructor->response [instructor]
  (if instructor
    (ok instructor)
    (not-found)))

(defn get-instructor [instructor-id]
  (-> (Instructor instructor-id)
      instructor->response))

(defn update-instructor [id updated-instructor]
  (db/update! Instructor id updated-instructor)
  (ok updated-instructor))

(defn delete-instructor [id]
  (db/delete! Instructor :id id)
  (ok))

