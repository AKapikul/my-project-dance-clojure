(ns my-project.dto.danceGroupDto
  (:require [schema.core :as schema]
            [my-project.model.danceGroup :refer [DanceGroup]]))

(schema/defschema DanceGroupDTO
                  {
                   :name           schema/Str
                   :startDate      schema/Any
                   :danceId        schema/Int
                   :instructorId   schema/Int
                   })
