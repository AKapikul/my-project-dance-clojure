(ns my-project.dto.dancerDto
  (:require [schema.core :as schema]
            [my-project.model.dancer :refer [Dancer]]))

(schema/defschema DancerDTO
                  {
                   :firstName      schema/Str
                   :lastName       schema/Str
                   :danceGroupId   schema/Int
                   })