(ns my-project.dto.instructorDto
  (:require [schema.core :as schema]
            [my-project.model.instructor :refer [Instructor]]))

(schema/defschema InstructorDTO
                  {
                   :firstName    schema/Str
                   :lastName     schema/Str
                   :danceId      schema/Int
                   })