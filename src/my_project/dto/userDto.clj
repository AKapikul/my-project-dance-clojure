(ns my-project.dto.userDto
  (:require [schema.core :as schema]
            [my-project.model.user :refer [User]]))

(schema/defschema UserDTO
  {:username  schema/Str
   :password  schema/Str
   :firstName schema/Str
   :lastName  schema/Str
   :email     schema/Str})