(ns my-project.dto.danceDto
  (:require [schema.core :as schema]
            [my-project.model.dance :refer [Dance]]))

(schema/defschema DanceDTO
                  {
                   :name                        schema/Str
                   :note                        schema/Str
                   })
