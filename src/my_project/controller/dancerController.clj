(ns my-project.controller.dancerController
  (:require [my-project.dto.dancerDto :refer [DancerDTO]]
            [compojure.api.sweet :refer [GET POST PUT DELETE]]
            [my-project.service.dancerService :refer [create-dancer
                                                     get-dancers
                                                     get-dancer
                                                     update-dancer
                                                     delete-dancer]]
            [schema.core :as schema]))

(def dancer-routes
  [(POST "/dancer" []
     :tags ["dancer"]
     :body [add-dancer-req DancerDTO]
     :summary "Create new dancer"
     :return [DancerDTO]
     (create-dancer add-dancer-req))
   (GET "/dancer/all" []
     :tags ["dancer"]
     :summary "Get all dancers"
     (get-dancers))
   (GET "/dancer/:id" []
     :path-params [id :- schema/Int]
     :summary "Get dancer by id"
     :tags ["dancer"]
     (get-dancer id))
   (PUT "/dancer/:id" []
      :tags ["dancer"]
      :summary "Update dancer by id"
      :path-params [id :- schema/Int]
      :body [updated-dancer DancerDTO]
      (update-dancer id updated-dancer))
   (DELETE "/dancer/:id" []
     :tags ["dancer"]
     :summary "Delete dancer by id"
     :path-params [id :- schema/Int]
     (delete-dancer id))])
