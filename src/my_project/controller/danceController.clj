(ns my-project.controller.danceController
  (:require [my-project.dto.danceDto :refer [DanceDTO]]
            [compojure.api.sweet :refer [GET POST PUT DELETE]]
            [my-project.service.danceService :refer [create-dance
                                                     get-dances
                                                     get-dance
                                                     update-dance
                                                     delete-dance]]
            [schema.core :as schema]))

(def dance-routes
  [(POST "/dance" []
     :tags ["dance"]
     :body [add-dance-req DanceDTO]
     :summary "Create new dance"
     :return [DanceDTO]
     (create-dance add-dance-req))
   (GET "/dance/all" []
     :tags ["dance"]
     :summary "Get all dances"
     (get-dances))
   (GET "/dance/:id" []
     :path-params [id :- schema/Int]
     :summary "Get dance by id"
     :tags ["dance"]
     (get-dance id))
   (PUT "/dance/:id" []
     :tags ["dance"]
     :summary "Update dance by id"
     :path-params [id :- schema/Int]
     :body [updated-dance DanceDTO]
     (update-dance id updated-dance))
   (DELETE "/dance/:id" []
     :tags ["dance"]
     :summary "Delete dance by id"
     :path-params [id :- schema/Int]
     (delete-dance id))])
