(ns my-project.controller.userController
  (:require [my-project.dto.userDto :refer [UserDTO]]
            [compojure.api.sweet :refer [GET POST PUT DELETE]]
            [my-project.service.userService :refer [create-user
                                                    get-users
                                                    get-user
                                                    update-user
                                                    delete-user]]
            [schema.core :as schema]))

(def user-routes
   [(POST "/user" []
       :tags ["user"]
       :body [add-user-req UserDTO]
       :summary "Create new user"
       :return [UserDTO]
       (create-user add-user-req))
    (GET "/user/all" []
       :tags ["user"]
       :summary "Get all users"
       (get-users))
    (GET "/user/:id" []
       :path-params [id :- schema/Int]
       :summary "Get user by id"
       :tags ["user"]
       (get-user id))
    (PUT "/user/:id" []
        :tags ["user"]
        :summary "Update user by id"
        :path-params [id :- schema/Int]
        :body [updated-user UserDTO]
        (update-user id updated-user))
    (DELETE "/user/:id" []
       :tags ["user"]
       :summary "Delete user by id"
       :path-params [id :- schema/Int]
       (delete-user id))])
