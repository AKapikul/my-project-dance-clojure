(ns my-project.controller.danceGroupController
  (:require [my-project.dto.danceGroupDto :refer [DanceGroupDTO]]
            [compojure.api.sweet :refer [GET POST PUT DELETE]]
            [my-project.service.danceGroupService :refer [create-danceGroup
                                                     get-danceGroups
                                                     get-danceGroup
                                                     update-danceGroup
                                                     delete-danceGroup]]
            [schema.core :as schema]))

(def danceGroup-routes
  [(POST "/danceGroup" []
     :tags ["danceGroup"]
     :body [add-danceGroup-req DanceGroupDTO]
     :summary "Create new danceGroup"
     :return [DanceGroupDTO]
     (create-danceGroup add-danceGroup-req))
   (GET "/danceGroup/all" []
     :tags ["danceGroup"]
     :summary "Get all danceGroups"
     (get-danceGroups))
   (GET "/danceGroup/:id" []
     :path-params [id :- schema/Int]
     :summary "Get danceGroup by id"
     :tags ["danceGroup"]
     (get-danceGroup id))
   (PUT "/danceGroup/:id" []
      :tags ["danceGroup"]
      :summary "Update dance group by id"
      :path-params [id :- schema/Int]
      :body [updated-danceGroup DanceGroupDTO]
      (update-danceGroup id updated-danceGroup))
   (DELETE "/danceGroup/:id" []
     :tags ["danceGroup"]
     :summary "Delete danceGroup by id"
     :path-params [id :- schema/Int]
     (delete-danceGroup id))])

