(ns my-project.controller.instructorController
  (:require [my-project.dto.instructorDto :refer [InstructorDTO]]
            [compojure.api.sweet :refer [GET POST PUT DELETE]]
            [my-project.service.instructorService :refer [create-instructor
                                                          get-instructors
                                                          get-instructor
                                                          update-instructor
                                                          delete-instructor]]
            [schema.core :as schema]))

(def instructor-routes
  [(POST "/instructor" []
     :tags ["instructor"]
     :body [add-instructor-req InstructorDTO]
     :summary "Create new instructor"
     :return [InstructorDTO]
     (create-instructor add-instructor-req))
   (GET "/instructor/all" []
     :tags ["instructor"]
     :summary "Get all instructors"
     (get-instructors))
   (GET "/instructor/:id" []
     :path-params [id :- schema/Int]
     :summary "Get instructor by id"
     :tags ["instructor"]
     (get-instructor id))
   (PUT "/instructor/:id" []
     :tags ["instructor"]
     :summary "Update instructor by id"
     :path-params [id :- schema/Int]
     :body [updated-instructor InstructorDTO]
     (update-instructor id updated-instructor))
   (DELETE "/instructor/:id" []
     :tags ["instructor"]
     :summary "Delete instructor by id"
     :path-params [id :- schema/Int]
     (delete-instructor id))])
