(ns my-project.core
  (:require [toucan.db :as db]
            [toucan.models :as models]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.util.http-response :refer :all]
            [compojure.api.sweet :refer [api routes]]
            [my-project.controller.userController :refer [user-routes]]
            [my-project.controller.danceController :refer [dance-routes]]
            [my-project.controller.dancerController :refer [dancer-routes]]
            [my-project.controller.danceGroupController :refer [danceGroup-routes]]
            [my-project.controller.instructorController :refer [instructor-routes]])
  (:gen-class))


(def database-spec
  {:classname   "com.mysql.cj.jdbc.Driver"
   :subprotocol "mysql"
   :subname     "//localhost:3306/dancedb"
   :user        "root"
   :password    "password"
   :useSSL      false
   })

(def swagger-config
  {
   :ui      "/swagger"
   :spec    "/swagger.json"
   :options {:ui   {:validatorUrl nil}
             :data {:info {:version "1.0.0", :title "Dance project"}}}

   })

(def app
  (-> (api {:swagger swagger-config} (apply routes user-routes dance-routes dancer-routes danceGroup-routes instructor-routes))
      (wrap-cors :access-control-allow-origin #"http://localhost:4200"
                 :access-control-allow-methods [:get :put :delete :post])))


(defn -main
  [& args]
  (db/set-default-db-connection! database-spec)
  (db/set-default-quoting-style! :mysql)
  (models/set-root-namespace! `my-project.model)
  (run-jetty app {:port 3000})
  (println "Dance school server started!"))
